import 'package:demotaskhotels/models/hotel.dart';
import 'package:demotaskhotels/models/room.dart';
import 'package:demotaskhotels/register.dart';
import 'package:demotaskhotels/settings.dart';
import 'package:dio/dio.dart';
import 'package:flutter_network_connectivity/flutter_network_connectivity.dart';

// Контроллер страницы номеров отеля
class RoomsController {
  final Hotel hotel; // был бы нужен если бы мы получали реальную информацию об отеле
  RoomsController(this.hotel);

  Future<List<Room>> load() async {
    final Dio dio =
        Dio(BaseOptions(connectTimeout: const Duration(seconds: 5), receiveTimeout: const Duration(seconds: 5)));

    if (!await FlutterNetworkConnectivity().isInternetConnectionAvailable()) {
      return Future.error('Нет подключения к интернету');
    }
    try {
      final Response response = await dio.get(getIt<Settings>().roomsUrl);
      return RoomList.getFromJson(response.data);
    } catch (e) {
      // Не будем пока детализировать ошибку
      return Future.error('Что-то пошло не так');
    }
  }
}
