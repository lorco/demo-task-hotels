import 'package:demotaskhotels/models/booking.dart';
import 'package:demotaskhotels/models/person.dart';
import 'package:demotaskhotels/models/room.dart';
import 'package:demotaskhotels/pages/paid_page.dart';
import 'package:demotaskhotels/register.dart';
import 'package:demotaskhotels/settings.dart';
import 'package:dio/dio.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_network_connectivity/flutter_network_connectivity.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

// Контроллер страницы бронирования
class BookingController with ChangeNotifier {
  final Room room; // было бы нужно если бы мы запрашивали реальную информацию о номере отеля
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormFieldState> phoneKey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> emailKey = GlobalKey<FormFieldState>();
  final TextEditingController textPhoneEditingController = TextEditingController();
  final TextEditingController textEmailEditingController = TextEditingController();
  final FocusNode phoneFocusNode = FocusNode();
  final FocusNode emailFocusNode = FocusNode();

  Booking? booking;
  Buyer buyer = Buyer();
  List<Person> persons = [Person()];
  String defPhoneMask = '+7 (***) ***-**-**';

  final MaskTextInputFormatter maskFormatter = MaskTextInputFormatter(
    mask: '+7 (***) ***-**-**',
    filter: {"*": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.eager,
  );

  BookingController(this.room) {
    phoneFocusNode.addListener(phoneFocusChange);
    emailFocusNode.addListener(emailFocusChange);
  }

  // Загрузка данных о бронировании
  Future<Booking> load() async {
    final Dio dio =
        Dio(BaseOptions(connectTimeout: const Duration(seconds: 5), receiveTimeout: const Duration(seconds: 5)));

    if (!await FlutterNetworkConnectivity().isInternetConnectionAvailable()) {
      return Future.error('Нет подключения к интернету');
    }
    try {
      final Response response = await dio.get(getIt<Settings>().bookingUrl);
      booking = Booking.fromMap(response.data);
      return Future.value(booking);
    } catch (e) {
      // Не будем пока детализировать ошибку
      return Future.error('Что-то пошло не так');
    }
  }

  // добавление туриста
  addPerson() {
    persons.add(Person());
    notifyListeners();
  }

  // оплатить
  pay(BuildContext context) {
    // валидация формы
    if (!formKey.currentState!.validate()) {
      if (!context.mounted) return;
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: Theme.of(context).colorScheme.error,
          duration: const Duration(seconds: 2),
          content: const Text('Проверьте ошибки заполнения формы')));
      return;
    }

    // По ТЗ данные мы никуда не отправляем, но "собрать" их стоит
    buyer.phone = textPhoneEditingController.text;
    buyer.email = textEmailEditingController.text;
    booking?.buyer = buyer;
    booking?.persons = persons;

    // Как будто данные отправлены, и оплата прошла
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PaidPage(),
      ),
    );
  }

  // Слежка за фокусом в поле телефона
  void phoneFocusChange() {
    if (phoneFocusNode.hasFocus && textPhoneEditingController.text.isEmpty) {
      textPhoneEditingController.text = defPhoneMask;
      textPhoneEditingController.selection = TextSelection.fromPosition(const TextPosition(offset: 4));
    }
    if (!phoneFocusNode.hasFocus) {
      phoneKey.currentState?.validate();
    }
  }

  // Изменения в поле телефона
  void changePhone() {
    String replacement = maskFormatter.getMaskedText();
    textPhoneEditingController.text = defPhoneMask.replaceRange(0, replacement.length, replacement);
    textPhoneEditingController.selection = TextSelection.fromPosition(TextPosition(offset: replacement.length));
  }

  // Слежка за фокусом в поле почты
  void emailFocusChange() {
    if (!emailFocusNode.hasFocus) {
      emailKey.currentState?.validate();
    }
  }

  phoneValidate() {
    String value = maskFormatter.getUnmaskedText();
    if (value.isEmpty) {
      return 'Введите номер телефона';
    }
    if (value.length != 10) {
      return 'Некорректный номер телефона';
    }
    return null;
  }

  emailValidate(String? value) {
    if (value == null || value.isEmpty) {
      return 'Введите email';
    }
    if (!EmailValidator.validate(value)) {
      return 'Некорректный email';
    }
    return null;
  }

  // Обязательно вызвать в dispose виджета, где используется контроллер
  void close() {
    textPhoneEditingController.dispose();
    textEmailEditingController.dispose();
    phoneFocusNode.dispose();
    emailFocusNode.dispose();
  }
}
