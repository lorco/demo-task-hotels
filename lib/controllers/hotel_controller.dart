import 'package:demotaskhotels/models/hotel.dart';
import 'package:demotaskhotels/register.dart';
import 'package:demotaskhotels/settings.dart';
import 'package:dio/dio.dart';
import 'package:flutter_network_connectivity/flutter_network_connectivity.dart';

// Контроллер страницы отеля
class HotelController {
  Future<Hotel> load() async {
    final Dio dio =
        Dio(BaseOptions(connectTimeout: const Duration(seconds: 5), receiveTimeout: const Duration(seconds: 5)));

    if (!await FlutterNetworkConnectivity().isInternetConnectionAvailable()) {
      return Future.error('Нет подключения к интернету');
    }
    try {
      final Response response = await dio.get(getIt<Settings>().hotelUrl);
      return Hotel.fromMap(response.data);
    } catch (e) {
      // Не будем пока детализировать ошибку
      return Future.error('Что-то пошло не так');
    }
  }
}
