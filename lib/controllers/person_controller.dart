import 'package:demotaskhotels/models/person.dart';
import 'package:demotaskhotels/utils.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

// Контроллер формы данных туриста
class PersonController with ChangeNotifier {
  final Person person;
  final int index;
  final GlobalKey personKey = GlobalKey();
  final GlobalKey<FormFieldState> nameKey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> lastNameKey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> birthdayKey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> citizenshipKey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> passportNumberKey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> expirationDateKey = GlobalKey<FormFieldState>();

  final TextEditingController textNameEditingController = TextEditingController();
  final TextEditingController textLastNameEditingController = TextEditingController();
  final TextEditingController textBirthdayEditingController = TextEditingController();
  final TextEditingController textCitizenshipEditingController = TextEditingController();
  final TextEditingController textPassportNumberEditingController = TextEditingController();
  final TextEditingController textExpirationDateEditingController = TextEditingController();

  final FocusNode nameFocusNode = FocusNode();
  final FocusNode lastNameFocusNode = FocusNode();
  final FocusNode birthdayFocusNode = FocusNode();
  final FocusNode citizenshipFocusNode = FocusNode();
  final FocusNode passportNumberFocusNode = FocusNode();
  final FocusNode expirationDateFocusNode = FocusNode();

  // Маска, которая немного усложнит ввод некорректной даты
  final MaskTextInputFormatter dateMaskFormatter = MaskTextInputFormatter(
    mask: 'dD.mM.yY**',
    filter: {
      "d": RegExp('[0-3]'),
      "D": RegExp('[0-9]'),
      "m": RegExp('[0-1]'),
      "M": RegExp('[0-9]'),
      "y": RegExp('[1-2]'),
      "Y": RegExp('[09]'),
      "*": RegExp('[0-9]'),
    },
    type: MaskAutoCompletionType.eager,
  );

  bool isDropDown = true;

  PersonController(this.person, this.index) {
    textNameEditingController.addListener(() {
      person.name = textNameEditingController.text;
    });
    textLastNameEditingController.addListener(() {
      person.lastname = textLastNameEditingController.text;
    });
    textBirthdayEditingController.addListener(() {
      person.birthday = textBirthdayEditingController.text;
    });
    textCitizenshipEditingController.addListener(() {
      person.citizenship = textCitizenshipEditingController.text;
    });
    textPassportNumberEditingController.addListener(() {
      person.passportNumber = textPassportNumberEditingController.text;
    });
    textExpirationDateEditingController.addListener(() {
      person.expirationDate = textExpirationDateEditingController.text;
    });

    nameFocusNode.addListener(focusChange(nameFocusNode, nameKey));
    lastNameFocusNode.addListener(focusChange(lastNameFocusNode, lastNameKey));
    birthdayFocusNode.addListener(focusChange(birthdayFocusNode, birthdayKey));
    citizenshipFocusNode.addListener(focusChange(citizenshipFocusNode, citizenshipKey));
    passportNumberFocusNode.addListener(focusChange(passportNumberFocusNode, passportNumberKey));
    expirationDateFocusNode.addListener(focusChange(expirationDateFocusNode, expirationDateKey));
  }

  focusChange(FocusNode focusNode, GlobalKey<FormFieldState> key) => () {
        if (!focusNode.hasFocus) {
          key.currentState?.validate();
        }
      };

  toggleDropDown() {
    isDropDown = !isDropDown;
    notifyListeners();
  }

  textValidate(String? value) {
    if (value == null || value.isEmpty) {
      isDropDown = true;
      notifyListeners();
      return 'Заполните поле';
    }
  }

  dateValidate(String? value) {
    if (value == null || value.isEmpty) {
      isDropDown = true;
      notifyListeners();
      return 'Заполните поле';
    }
    // Можно было бы доп. валидировать отдельно дату рождения и срок действия паспорта, если бы это было в ТЗ
    if (!isDateValid(value)) {
      isDropDown = true;
      notifyListeners();
      return 'Некорректная дата';
    }
  }

  // Обязательно вызвать в dispose виджета, где используется контроллер
  void close() {
    textNameEditingController.dispose();
    textLastNameEditingController.dispose();
    textBirthdayEditingController.dispose();
    textCitizenshipEditingController.dispose();
    textPassportNumberEditingController.dispose();
    textExpirationDateEditingController.dispose();
    nameFocusNode.dispose();
    lastNameFocusNode.dispose();
    birthdayFocusNode.dispose();
    citizenshipFocusNode.dispose();
    passportNumberFocusNode.dispose();
    expirationDateFocusNode.dispose();
  }
}
