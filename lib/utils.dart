import 'package:money_formatter/money_formatter.dart';
import 'package:intl/intl.dart';

// Форматирование строки валюты
String currencyFormat(int price) {
  return MoneyFormatter(
    amount: price.toDouble(),
    settings: MoneyFormatterSettings(
      symbol: '₽',
      thousandSeparator: ' ',
      decimalSeparator: ',',
      symbolAndNumberSeparator: ' ',
      fractionDigits: 0,
    ),
  ).output.symbolOnRight;
}

bool isDateValid(String date, [String? separator]) {
  separator ??= '.';
  try {
    DateFormat('dd${separator}MM${separator}yyyy').parseStrict(date);
    return true;
  } catch (e) {
    return false;
  }
}
