import 'package:demotaskhotels/register.dart';
import 'package:flutter/material.dart';
import 'package:demotaskhotels/pages/hotel_page.dart';
import 'package:demotaskhotels/theme.dart';

void main() {
  register();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hotels',
      theme: MainTheme.get(),
      home: const HotelPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
