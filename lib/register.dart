import 'package:demotaskhotels/settings.dart';
import 'package:demotaskhotels/theme.dart';
import 'package:get_it/get_it.dart';

// Регистрация глобально используемых объектов (синглетонов) с возможностью подменять на тестовые при необходимости
final getIt = GetIt.instance;

void register() {
  getIt.registerSingleton<Settings>(Settings());
  getIt.registerSingleton<AppDecoration>(AppDecoration());
}
