class Settings {
  String get hotelUrl => 'https://run.mocky.io/v3/d144777c-a67f-4e35-867a-cacc3b827473';
  String get roomsUrl => 'https://run.mocky.io/v3/8b532701-709e-4194-a41c-1a903af00195';
  String get bookingUrl => 'https://run.mocky.io/v3/63866c74-d593-432c-af8e-f279d1a8d2ff';
}
