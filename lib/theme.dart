import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MainTheme {
  static const Color textColor = Colors.black;
  static const Color surfaceColor = Colors.white;
  static const Color scaffoldBackgroundColor = Color(0xFFF6F6F9);
  static const Color primaryColor = Color(0xFF0D72FF);

  static ThemeData get() {
    return ThemeData(
      brightness: Brightness.light,
      fontFamily: 'SF-Pro-Display',
      scaffoldBackgroundColor: scaffoldBackgroundColor,
      colorScheme: const ColorScheme.light(primary: primaryColor),
      textTheme: const TextTheme(
        titleLarge: TextStyle(fontSize: 30.0, color: textColor, fontWeight: FontWeight.w600),
        titleMedium: TextStyle(fontSize: 22.0, color: textColor, fontWeight: FontWeight.w500),
        bodyMedium: TextStyle(fontSize: 16.0, color: Color(0xFF2C3035), fontWeight: FontWeight.w400),
        labelLarge: TextStyle(fontSize: 16.0, color: Color(0xFF828796), fontWeight: FontWeight.w400),
        labelMedium: TextStyle(fontSize: 14.0, color: Color(0xFF828796), fontWeight: FontWeight.w400),
      ),
      appBarTheme: const AppBarTheme(
        color: surfaceColor,
        iconTheme: IconThemeData(
          color: textColor,
        ),
        titleTextStyle: TextStyle(color: textColor, fontSize: 20),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          fixedSize: MaterialStateProperty.all(const Size(500, 48)),
          textStyle: MaterialStateProperty.all(const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500)),
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        ),
      ),
    );
  }
}

extension ThemeColors on ColorScheme {
  Color get rating => const Color(0xFFFFA800);
  Color get neutralColor0 => const Color(0xFFFBFBFC);
  Color get neutralColor1 => const Color(0xFFF6F6F9);
  Color get neutralColor2 => const Color(0xFFCCCCCC);
  Color get neutralColor3 => const Color(0xFFA9ABB7);
  Color get neutralColor4 => const Color(0xFF828796);
  Color get neutralColor5 => const Color(0xFF2C3035);
  Color get error => const Color(0xFFEB5757);
  Color get errorFill => const Color(0xFFEB5757).withOpacity(0.15);
}

class AppDecoration {
  BoxDecoration blockDecoration(BuildContext context) => BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Theme.of(context).cardColor,
      );

  // Поле ввода
  Widget input(
    BuildContext context, {
    required TextEditingController controller,
    required String text,
    Key? key,
    FocusNode? focusNode,
    FormFieldValidator<String>? validator,
    List<TextInputFormatter>? formatters,
    ValueChanged<String>? onChanged,
    TextInputType keyboardType = TextInputType.text,
  }) {
    return TextFormField(
      key: key,
      controller: controller,
      decoration: InputDecoration(
        label: Text(text),
        labelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 17,
          color: Theme.of(context).colorScheme.neutralColor3,
        ),
        fillColor: MaterialStateColor.resolveWith((Set<MaterialState> states) {
          if (states.contains(MaterialState.error)) {
            return Theme.of(context).colorScheme.errorFill;
          }
          return Theme.of(context).colorScheme.neutralColor1;
        }),
        filled: true,
        errorStyle: TextStyle(color: Theme.of(context).colorScheme.error),
        contentPadding: const EdgeInsets.all(15.0),
        enabledBorder: getOutlineInputBorder(context),
        focusedBorder: getOutlineInputBorder(context),
        errorBorder: getOutlineInputBorder(context),
        focusedErrorBorder: getOutlineInputBorder(context),
      ),
      style: Theme.of(context).textTheme.bodyMedium,
      keyboardType: keyboardType,
      validator: validator,
      inputFormatters: formatters,
      focusNode: focusNode,
      onChanged: onChanged,
    );
  }

  // Оформление границ поля ввода комментария
  InputBorder getOutlineInputBorder(BuildContext context, {bool error = false}) => UnderlineInputBorder(
        borderRadius: BorderRadius.circular(10),
        borderSide: BorderSide(
            color: error ? Theme.of(context).colorScheme.errorFill : Theme.of(context).colorScheme.neutralColor1),
      );
}
