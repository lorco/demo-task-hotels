class Person {
  String? name;
  String? lastname;
  String? birthday;
  String? citizenship;
  String? passportNumber;
  String? expirationDate;

  Person();
}
