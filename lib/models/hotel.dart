class Hotel {
  int id;
  String name;
  String address;
  int minimalPrice;
  String priceForIt;
  int rating;
  String ratingName;
  List imageUrls = [];
  AboutHotel aboutHotel;

  Hotel(
    this.id,
    this.name,
    this.address,
    this.minimalPrice,
    this.priceForIt,
    this.rating,
    this.ratingName,
    this.imageUrls,
    this.aboutHotel,
  );

  Hotel.fromMap(Map<String, dynamic> map)
      : id = map['id'].toInt(),
        name = map['name'],
        address = map['adress'],
        minimalPrice = map['minimal_price'].toInt(),
        priceForIt = map['price_for_it'],
        rating = map['rating'].toInt(),
        ratingName = map['rating_name'],
        imageUrls = map['image_urls'] as List,
        aboutHotel = AboutHotel.fromMap(map['about_the_hotel'] as Map<String, dynamic>);
}

class AboutHotel {
  String description;
  List peculiarities;

  AboutHotel.fromMap(Map<String, dynamic> map)
      : description = map['description'],
        peculiarities = map['peculiarities'] as List;
}
