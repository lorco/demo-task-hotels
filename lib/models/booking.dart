import 'package:demotaskhotels/models/person.dart';

// Бронирование
class Booking {
  int id;
  String hotelName;
  String hotelAdress;
  int horating;
  String ratingName;
  String departure;
  String arrivalCountry;
  String tourDateStart;
  String tourDateStop;
  int numberOfNights;
  String room;
  String nutrition;
  int tourPrice;
  int fuelCharge;
  int serviceCharge;
  int get totalAmount => tourPrice + fuelCharge + serviceCharge;
  Buyer? buyer;
  List<Person>? persons;

  Booking.fromMap(Map<String, dynamic> map)
      : id = map['id'].toInt(),
        hotelName = map['hotel_name'],
        hotelAdress = map['hotel_adress'],
        horating = map['horating'].toInt(),
        ratingName = map['rating_name'],
        departure = map['departure'],
        arrivalCountry = map['arrival_country'],
        tourDateStart = map['tour_date_start'],
        tourDateStop = map['tour_date_stop'],
        numberOfNights = map['number_of_nights'].toInt(),
        room = map['room'],
        nutrition = map['nutrition'],
        tourPrice = map['tour_price'].toInt(),
        fuelCharge = map['fuel_charge'].toInt(),
        serviceCharge = map['service_charge'].toInt();
}

// Покупатель
class Buyer {
  // мы не знаем, в каком формате в конечном итоге нам нужен телефон, как он будет использоваться, этого нет в ТЗ
  String? phone;
  String? email;
}
