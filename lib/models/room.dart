class Room {
  int id;
  String name;
  int price;
  String pricePer;
  List peculiarities;
  List imageUrls = [];

  Room(
    this.id,
    this.name,
    this.price,
    this.pricePer,
    this.peculiarities,
    this.imageUrls,
  );

  Room.fromMap(Map<String, dynamic> map)
      : id = map['id'].toInt(),
        name = map['name'],
        price = map['price'].toInt(),
        pricePer = map['price_per'],
        peculiarities = map['peculiarities'] as List,
        imageUrls = map['image_urls'] as List;
}

class RoomList {
  // из json
  static List<Room> getFromJson(Map<String, dynamic> json) {
    return (json['rooms'] as List).map((e) => Room.fromMap(e)).toList();
  }
}
