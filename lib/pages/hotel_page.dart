import 'package:demotaskhotels/controllers/hotel_controller.dart';
import 'package:demotaskhotels/models/hotel.dart';
import 'package:demotaskhotels/pages/rooms_page.dart';
import 'package:demotaskhotels/register.dart';
import 'package:demotaskhotels/utils.dart';
import 'package:demotaskhotels/widgets/carousel_widget.dart';
import 'package:demotaskhotels/widgets/label_widget.dart';
import 'package:demotaskhotels/widgets/rating_widget.dart';
import 'package:flutter/material.dart';
import 'package:demotaskhotels/theme.dart';

// Страница отеля
class HotelPage extends StatefulWidget {
  const HotelPage({this.controller, Key? key}) : super(key: key);
  final HotelController? controller;

  @override
  State<HotelPage> createState() => _HotelPageState();
}

class _HotelPageState extends State<HotelPage> {
  late HotelController controller;
  Future<Hotel>? hotelData;

  @override
  void initState() {
    controller = widget.controller ?? HotelController();
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    hotelData = controller.load();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: const Text('Отель'),
      ),
      body: Center(
        child: FutureBuilder<Hotel>(
          future: hotelData,
          builder: (context, snapshot) {
            if (hotelData == null) {
              return const Text('Нет данных');
            } else {
              return snapshot.hasData
                  ? ListView(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.only(
                              bottomRight: Radius.circular(15.0),
                              bottomLeft: Radius.circular(15.0),
                            ),
                            color: Theme.of(context).cardColor,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10.0, right: 16.0, bottom: 20.0, left: 16.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // карусель фото
                                CarouselPhotoSlider(snapshot.requireData.imageUrls),
                                const SizedBox(height: 10),

                                // основные данные об отеле
                                RatingWidget(
                                  rating: snapshot.requireData.rating,
                                  title: snapshot.requireData.ratingName,
                                ),
                                const SizedBox(height: 8),
                                Text(snapshot.requireData.name, style: Theme.of(context).textTheme.titleMedium),
                                TextButton(
                                  style: TextButton.styleFrom(padding: const EdgeInsets.all(0.0)),
                                  onPressed: () {},
                                  child: Text(
                                    snapshot.requireData.address,
                                    style: TextStyle(
                                      color: Theme.of(context).colorScheme.primary,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 3),
                                Wrap(
                                  crossAxisAlignment: WrapCrossAlignment.end,
                                  children: [
                                    Text(
                                      'от ${currencyFormat(snapshot.requireData.minimalPrice)}',
                                      style: Theme.of(context).textTheme.titleLarge,
                                    ),
                                    const SizedBox(width: 12),
                                    Text(
                                      snapshot.requireData.priceForIt,
                                      style: Theme.of(context).textTheme.labelLarge,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),

                        const SizedBox(height: 14),

                        // подробные данные об отеле
                        Container(
                          decoration: getIt<AppDecoration>().blockDecoration(context),
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Об отеле', style: Theme.of(context).textTheme.titleMedium),
                                const SizedBox(height: 8),
                                Wrap(
                                  children: snapshot.requireData.aboutHotel.peculiarities
                                      .map((item) => LabelWidget(item))
                                      .toList(),
                                ),
                                const SizedBox(height: 10),
                                Text(
                                  snapshot.requireData.aboutHotel.description,
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                                const SizedBox(height: 18),
                                Card(
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  color: Theme.of(context).colorScheme.neutralColor0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      top: 18.0,
                                      right: 10.0,
                                      bottom: 18.0,
                                      left: 16.0,
                                    ),
                                    child: Column(
                                      children: [
                                        infoButton(
                                          image: 'assets/images/emoji-happy.png',
                                          title: 'Удобства',
                                          subtitle: 'Самое необходимое',
                                        ),
                                        const Padding(
                                          padding: EdgeInsets.only(top: 4.0, left: 38.0, right: 10.0, bottom: 4.0),
                                          child: Divider(),
                                        ),
                                        infoButton(
                                          image: 'assets/images/tick-square.png',
                                          title: 'Что включено',
                                          subtitle: 'Самое необходимое',
                                        ),
                                        const Padding(
                                          padding: EdgeInsets.only(top: 4.0, left: 38.0, right: 10.0, bottom: 4.0),
                                          child: Divider(),
                                        ),
                                        infoButton(
                                          image: 'assets/images/close-square.png',
                                          title: 'Что не включено',
                                          subtitle: 'Самое необходимое',
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),

                        const SizedBox(height: 14),

                        // кнопка выбора номера
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.only(
                              topRight: Radius.circular(15.0),
                              topLeft: Radius.circular(15.0),
                            ),
                            color: Theme.of(context).cardColor,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: ElevatedButton(
                              onPressed: () {
                                // Пока не будем ничего мудрить с навигацией во имя экономии времени воспользуемся самым простым способом
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => RoomsPage(hotel: snapshot.requireData)),
                                );
                              },
                              child: const Text('К выбору номера'),
                            ),
                          ),
                        ),
                      ],
                    )
                  : snapshot.hasError
                      ? Text(snapshot.error.toString())
                      : const CircularProgressIndicator();
            }
          },
        ),
      ),
    );
  }

  Widget infoButton({required String image, required String title, required String subtitle}) {
    return Row(
      children: [
        Image(image: AssetImage(image), width: 24),
        const SizedBox(width: 12),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(
                  color: Theme.of(context).colorScheme.neutralColor5,
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: 3),
              Text(
                subtitle,
                style: TextStyle(
                  color: Theme.of(context).colorScheme.neutralColor4,
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
        Icon(
          Icons.chevron_right,
          size: 35,
          color: Theme.of(context).colorScheme.neutralColor5,
        ),
      ],
    );
  }
}
