import 'package:demotaskhotels/controllers/rooms_controller.dart';
import 'package:demotaskhotels/models/hotel.dart';
import 'package:demotaskhotels/models/room.dart';
import 'package:demotaskhotels/widgets/room_widget.dart';
import 'package:flutter/material.dart';

// Страница - список номеров отеля
class RoomsPage extends StatefulWidget {
  const RoomsPage({required this.hotel, this.controller, Key? key}) : super(key: key);
  final Hotel hotel;
  final RoomsController? controller;

  @override
  State<RoomsPage> createState() => _RoomsPageState();
}

class _RoomsPageState extends State<RoomsPage> {
  late RoomsController controller;
  Future<List<Room>>? roomsData;

  @override
  void initState() {
    controller = widget.controller ?? RoomsController(widget.hotel);
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    roomsData = controller.load();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(widget.hotel.name),
        elevation: 0,
      ),
      body: Center(
        child: FutureBuilder<List<Room>>(
          future: roomsData,
          builder: (context, snapshot) {
            return roomsData == null
                ? const Text('Нет данных')
                : snapshot.hasData
                    ? ListView.builder(
                        physics: const BouncingScrollPhysics(),
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        itemCount: snapshot.requireData.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5.0),
                            child: RoomWidget(snapshot.requireData[index]),
                          );
                        },
                      )
                    : snapshot.hasError
                        ? Text(snapshot.error.toString())
                        : const CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
