import 'dart:math';
import 'package:demotaskhotels/pages/hotel_page.dart';
import 'package:demotaskhotels/theme.dart';
import 'package:flutter/material.dart';

class PaidPage extends StatelessWidget {
  PaidPage({Key? key}) : super(key: key);
  final int orderNumber = Random().nextInt(900000) + 100000;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
        title: const Text('Заказ оплачен'),
        elevation: 0,
      ),
      body: SafeArea(
        child: Container(
          color: Theme.of(context).cardColor,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 94,
                          height: 94,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Theme.of(context).colorScheme.neutralColor1,
                          ),
                          child: const Image(image: AssetImage('assets/images/party.png'), width: 44),
                        ),
                        const SizedBox(height: 26),
                        Text('Ваш заказ принят в работу', style: Theme.of(context).textTheme.titleMedium),
                        const SizedBox(height: 20),
                        Text(
                          'Подтверждение заказа № $orderNumber может занять некоторое время (от 1 часа до суток).\n'
                          'Как только мы получим ответ от туроператора, вам на почту придет уведомление.',
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.labelLarge,
                        ),
                        const SizedBox(height: 40),
                      ],
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.pushAndRemoveUntil<dynamic>(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (BuildContext context) => const HotelPage(),
                      ),
                      (route) => false,
                    );
                  },
                  child: const Text('Супер!'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
