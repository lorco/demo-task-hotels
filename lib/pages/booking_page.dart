import 'package:demotaskhotels/controllers/booking_controller.dart';
import 'package:demotaskhotels/models/booking.dart';
import 'package:demotaskhotels/models/room.dart';
import 'package:demotaskhotels/register.dart';
import 'package:demotaskhotels/theme.dart';
import 'package:demotaskhotels/utils.dart';
import 'package:demotaskhotels/widgets/person_widget.dart';
import 'package:demotaskhotels/widgets/rating_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// Страница бронирования
class BookingPage extends StatefulWidget {
  const BookingPage({required this.room, this.controller, Key? key}) : super(key: key);
  final Room room; // было бы нужно если бы мы запрашивали реальную информацию о номере отеля
  final BookingController? controller;

  @override
  State<BookingPage> createState() => _BookingPageState();
}

class _BookingPageState extends State<BookingPage> {
  late BookingController controller;
  Future<Booking>? bookingData;

  @override
  void initState() {
    controller = widget.controller ?? BookingController(widget.room);
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    bookingData = controller.load();
  }

  @override
  void dispose() {
    controller.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerDown: (_) => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context),
          ),
          title: const Text('Бронирование'),
          elevation: 0,
        ),
        body: Center(
          child: FutureBuilder<Booking>(
            future: bookingData,
            builder: (context, snapshot) {
              return bookingData == null
                  ? const Text('Нет данных')
                  : snapshot.hasData
                      ? SingleChildScrollView(
                          child: Form(
                            key: controller.formKey,
                            child: Column(
                              children: [
                                const SizedBox(height: 10),

                                // блок с отелем
                                Container(
                                  decoration: getIt<AppDecoration>().blockDecoration(context),
                                  child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        RatingWidget(
                                          rating: snapshot.requireData.horating,
                                          title: snapshot.requireData.ratingName,
                                        ),
                                        const SizedBox(height: 8),
                                        Text(snapshot.requireData.hotelName,
                                            style: Theme.of(context).textTheme.titleMedium),
                                        TextButton(
                                          style: TextButton.styleFrom(padding: const EdgeInsets.all(0.0)),
                                          onPressed: () {},
                                          child: Text(
                                            snapshot.requireData.hotelAdress,
                                            style: TextStyle(
                                              color: Theme.of(context).colorScheme.primary,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),

                                const SizedBox(height: 10),

                                // блок с данными брони
                                Container(
                                  decoration: getIt<AppDecoration>().blockDecoration(context),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                                    child: Column(
                                      children: [
                                        Table(
                                          columnWidths: const <int, TableColumnWidth>{
                                            0: FlexColumnWidth(1.2),
                                            1: FlexColumnWidth(1.8),
                                          },
                                          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                                          children: <TableRow>[
                                            TableRow(
                                              children: [
                                                textWrap('Вылет из', left: true),
                                                textWrap(snapshot.requireData.departure),
                                              ],
                                            ),
                                            TableRow(
                                              children: [
                                                textWrap('Страна, город', left: true),
                                                textWrap(snapshot.requireData.arrivalCountry),
                                              ],
                                            ),
                                            TableRow(
                                              children: [
                                                textWrap('Даты', left: true),
                                                textWrap(
                                                    '${snapshot.requireData.tourDateStart} - ${snapshot.requireData.tourDateStop}'),
                                              ],
                                            ),
                                            TableRow(
                                              children: [
                                                textWrap('Кол-во ночей', left: true),
                                                textWrap(
                                                  snapshot.requireData.numberOfNights.toString(),
                                                )
                                              ],
                                            ),
                                            TableRow(
                                              children: [
                                                textWrap('Отель', left: true),
                                                textWrap(snapshot.requireData.hotelName),
                                              ],
                                            ),
                                            TableRow(
                                              children: [
                                                textWrap('Номер', left: true),
                                                textWrap(snapshot.requireData.room),
                                              ],
                                            ),
                                            TableRow(
                                              children: [
                                                textWrap('Питание', left: true),
                                                textWrap(snapshot.requireData.nutrition),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),

                                const SizedBox(height: 10),

                                // блок с информацией о покупателе
                                Container(
                                  decoration: getIt<AppDecoration>().blockDecoration(context),
                                  child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Информация о покупателе', style: Theme.of(context).textTheme.titleMedium),
                                        const SizedBox(height: 24),
                                        Column(
                                          children: [
                                            getIt<AppDecoration>().input(
                                              context,
                                              controller: controller.textPhoneEditingController,
                                              text: 'Номер телефона',
                                              key: controller.phoneKey,
                                              formatters: [controller.maskFormatter],
                                              keyboardType: TextInputType.phone,
                                              validator: (String? value) => controller.phoneValidate(),
                                              focusNode: controller.phoneFocusNode,
                                              onChanged: (String value) => controller.changePhone(),
                                            ),
                                            const SizedBox(height: 12),
                                            getIt<AppDecoration>().input(
                                              context,
                                              controller: controller.textEmailEditingController,
                                              text: 'Почта',
                                              key: controller.emailKey,
                                              keyboardType: TextInputType.emailAddress,
                                              validator: (String? value) => controller.emailValidate(value),
                                              focusNode: controller.emailFocusNode,
                                            ),
                                            const SizedBox(height: 12),
                                            Text(
                                              'Эти данные никому не передаются. После оплаты мы вышлем чек на указанный вами номер и почту',
                                              style: Theme.of(context).textTheme.labelMedium,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),

                                const SizedBox(height: 10),

                                // список туристов
                                ChangeNotifierProvider.value(
                                  value: controller,
                                  child: Consumer<BookingController>(builder: (context, controller, child) {
                                    return ListView.separated(
                                      shrinkWrap: true,
                                      physics: const NeverScrollableScrollPhysics(),
                                      itemCount: controller.persons.length,
                                      separatorBuilder: (BuildContext context, int index) => const SizedBox(height: 10),
                                      itemBuilder: (context, index) {
                                        return PersonWidget(controller.persons[index], index);
                                      },
                                    );
                                  }),
                                ),

                                const SizedBox(height: 10),

                                // блок добавления новых туристов
                                Container(
                                  decoration: getIt<AppDecoration>().blockDecoration(context),
                                  child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text('Добавить туриста', style: Theme.of(context).textTheme.titleMedium),
                                        GestureDetector(
                                          onTap: () => controller.addPerson(),
                                          child: Container(
                                            height: 32,
                                            width: 32,
                                            decoration: BoxDecoration(
                                              color: Theme.of(context).colorScheme.primary,
                                              borderRadius: BorderRadius.circular(10.0),
                                            ),
                                            child: Icon(
                                              Icons.add,
                                              size: 20,
                                              color: Theme.of(context).cardColor,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),

                                const SizedBox(height: 10),

                                // блок с итоговой ценой
                                Container(
                                  decoration: getIt<AppDecoration>().blockDecoration(context),
                                  child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            textWrap('Тур', left: true),
                                            Text(currencyFormat(snapshot.requireData.tourPrice)),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            textWrap('Топливный сбор', left: true),
                                            Text(currencyFormat(snapshot.requireData.fuelCharge)),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            textWrap('Сервисный сбор', left: true),
                                            Text(currencyFormat(snapshot.requireData.serviceCharge)),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            textWrap('Итого', left: true),
                                            Text(
                                              currencyFormat(snapshot.requireData.totalAmount),
                                              style: TextStyle(color: Theme.of(context).colorScheme.primary),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),

                                const SizedBox(height: 14),

                                // кнопка оплаты
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.only(
                                      topRight: Radius.circular(15.0),
                                      topLeft: Radius.circular(15.0),
                                    ),
                                    color: Theme.of(context).cardColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: ElevatedButton(
                                      onPressed: () => controller.pay(context),
                                      child: Text('Оплатить ${currencyFormat(snapshot.requireData.totalAmount)}'),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : snapshot.hasError
                          ? Text(snapshot.error.toString())
                          : const CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }

  // Обертка для содержимого строки таблицы, чтобы добавить отступы
  Widget textWrap(String text, {bool left = false}) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 9.0),
        child: Text(text, style: left ? Theme.of(context).textTheme.labelLarge : null));
  }
}
