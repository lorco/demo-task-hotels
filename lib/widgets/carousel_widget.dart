import 'package:carousel_slider/carousel_slider.dart';
import 'package:demotaskhotels/widgets/photo_widget.dart';
import 'package:flutter/material.dart';

class CarouselPhotoSlider extends StatefulWidget {
  const CarouselPhotoSlider(this.items, {super.key});
  final List items;

  @override
  State<CarouselPhotoSlider> createState() => _CarouselPhotoSlider();
}

class _CarouselPhotoSlider extends State<CarouselPhotoSlider> {
  final double sliderHeight = 280;
  int currentIndex = 0;
  int circleItemMaxCount = 5;

  @override
  void initState() {
    if (widget.items.length < circleItemMaxCount) {
      circleItemMaxCount = widget.items.length;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        CarouselSlider(
          options: CarouselOptions(
              height: sliderHeight,
              viewportFraction: 1,
              initialPage: 0,
              enableInfiniteScroll: false,
              onPageChanged: (index, reason) {
                setState(() => currentIndex = index);
              }),
          items: widget.items
              .map(
                (item) => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 4.0),
                  child: PhotoWidget(item),
                ),
              )
              .toList(),
        ),
        if (widget.items.isNotEmpty)
          Positioned(
            bottom: 4,
            child: Card(
              elevation: 0,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0)),
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: Iterable<int>.generate(circleItemMaxCount).toList().map((index) {
                    return Container(
                      width: 9.0,
                      height: 9.0,
                      margin: const EdgeInsets.symmetric(horizontal: 4.0),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: index == currentIndex ? Colors.black : Colors.black.withOpacity(0.2),
                      ),
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
      ],
    );
  }
}
