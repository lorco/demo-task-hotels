import 'package:flutter/material.dart';
import 'package:demotaskhotels/theme.dart';

class LabelWidget extends StatelessWidget {
  const LabelWidget(this.text, {Key? key}) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: Theme.of(context).colorScheme.neutralColor0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
        child: Text(text, style: Theme.of(context).textTheme.labelLarge),
      ),
    );
  }
}
