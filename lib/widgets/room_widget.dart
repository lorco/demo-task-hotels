import 'package:demotaskhotels/models/room.dart';
import 'package:demotaskhotels/pages/booking_page.dart';
import 'package:demotaskhotels/register.dart';
import 'package:demotaskhotels/theme.dart';
import 'package:demotaskhotels/utils.dart';
import 'package:demotaskhotels/widgets/about_room_button_widget.dart';
import 'package:demotaskhotels/widgets/carousel_widget.dart';
import 'package:demotaskhotels/widgets/label_widget.dart';
import 'package:flutter/material.dart';

// Виджет номера отеля
class RoomWidget extends StatelessWidget {
  const RoomWidget(this.room, {Key? key}) : super(key: key);
  final Room room;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: getIt<AppDecoration>().blockDecoration(context),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // карусель фото
            CarouselPhotoSlider(room.imageUrls),
            const SizedBox(height: 10),

            // информация о номере
            Text(room.name, style: Theme.of(context).textTheme.titleMedium),
            const SizedBox(height: 8),
            Wrap(
              children: room.peculiarities.map((item) => LabelWidget(item)).toList(),
            ),
            const SizedBox(height: 6),
            AboutRoomButtonWidget(room),
            const SizedBox(height: 18),
            Wrap(
              crossAxisAlignment: WrapCrossAlignment.end,
              children: [
                Text(
                  currencyFormat(room.price),
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                const SizedBox(width: 12),
                Text(
                  room.pricePer,
                  style: Theme.of(context).textTheme.labelLarge,
                ),
              ],
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                // Пока не будем ничего мудрить с навигацией во имя экономии времени воспользуемся самым простым способом
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => BookingPage(room: room)),
                );
              },
              child: const Text('Выбрать номер'),
            ),
          ],
        ),
      ),
    );
  }
}
