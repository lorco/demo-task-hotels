import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:demotaskhotels/theme.dart';

class PhotoWidget extends StatelessWidget {
  const PhotoWidget(this.item, {Key? key}) : super(key: key);
  final String item;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15.0),
      child: CachedNetworkImage(
        imageUrl: item,
        placeholder: (context, url) => const Center(child: CircularProgressIndicator()),
        errorWidget: (context, url, error) => noPhoto(context),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget noPhoto(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: 100,
          height: 100,
          child: Icon(
            Icons.photo_camera,
            size: 90,
            color: Theme.of(context).colorScheme.neutralColor2,
          ),
        ),
        Text('Фото недоступно', style: TextStyle(color: Theme.of(context).colorScheme.neutralColor2)),
      ],
    );
  }
}
