import 'package:demotaskhotels/models/room.dart';
import 'package:flutter/material.dart';

// Кнопка, которая должна вести на страницу номера отеля
class AboutRoomButtonWidget extends StatelessWidget {
  const AboutRoomButtonWidget(this.room, {Key? key}) : super(key: key);
  final Room room; // Было бы нужно, если бы кнопка вела на страницу номера

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: Theme.of(context).colorScheme.primary.withOpacity(0.1),
      child: Padding(
        padding: const EdgeInsets.only(left: 10.0, top: 3.0, bottom: 3.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Подробнее о номере',
              style: TextStyle(
                color: Theme.of(context).colorScheme.primary,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
            Icon(Icons.chevron_right, size: 24, color: Theme.of(context).colorScheme.primary),
          ],
        ),
      ),
    );
  }
}
