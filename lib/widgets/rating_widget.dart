import 'package:demotaskhotels/theme.dart';
import 'package:flutter/material.dart';

class RatingWidget extends StatelessWidget {
  const RatingWidget({required this.rating, required this.title, Key? key}) : super(key: key);
  final int rating;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: Theme.of(context).colorScheme.rating.withOpacity(0.2),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(Icons.star, size: 16, color: Theme.of(context).colorScheme.rating),
            const SizedBox(width: 5),
            Text(
              rating.toString(),
              style: TextStyle(
                color: Theme.of(context).colorScheme.rating,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(width: 5),
            Text(
              title,
              style: TextStyle(
                color: Theme.of(context).colorScheme.rating,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
