import 'package:demotaskhotels/controllers/person_controller.dart';
import 'package:demotaskhotels/models/person.dart';
import 'package:demotaskhotels/register.dart';
import 'package:demotaskhotels/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// Виджет формы данных о туристе
class PersonWidget extends StatefulWidget {
  const PersonWidget(this.person, this.index, {Key? key}) : super(key: key);
  final Person person;
  final int index;

  @override
  State<PersonWidget> createState() => _PersonWidgetState();
}

class _PersonWidgetState extends State<PersonWidget> {
  late final PersonController controller;

  @override
  void initState() {
    controller = PersonController(widget.person, widget.index);
    super.initState();
  }

  @override
  void dispose() {
    controller.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: controller,
      child: Consumer<PersonController>(builder: (context, controller, child) {
        return Container(
          decoration: getIt<AppDecoration>().blockDecoration(context),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(getTitle(widget.index), style: Theme.of(context).textTheme.titleMedium),
                    Container(
                      height: 32,
                      width: 32,
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.primary.withOpacity(0.10),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: GestureDetector(
                        onTap: () => controller.toggleDropDown(),
                        child: Icon(
                          controller.isDropDown ? Icons.expand_less : Icons.expand_more,
                          size: 32,
                          color: Theme.of(context).colorScheme.primary,
                        ),
                      ),
                    ),
                  ],
                ),
                Offstage(
                  key: controller.personKey,
                  offstage: !controller.isDropDown,
                  child: Column(
                    children: [
                      const SizedBox(height: 24),
                      getIt<AppDecoration>().input(
                        context,
                        controller: controller.textNameEditingController,
                        text: 'Имя',
                        key: controller.nameKey,
                        focusNode: controller.nameFocusNode,
                        validator: (String? value) => controller.textValidate(value),
                      ),
                      const SizedBox(height: 12),
                      getIt<AppDecoration>().input(
                        context,
                        controller: controller.textLastNameEditingController,
                        text: 'Фамилия',
                        key: controller.lastNameKey,
                        focusNode: controller.lastNameFocusNode,
                        validator: (String? value) => controller.textValidate(value),
                      ),
                      const SizedBox(height: 12),
                      getIt<AppDecoration>().input(
                        context,
                        controller: controller.textBirthdayEditingController,
                        text: 'Дата рождения',
                        key: controller.birthdayKey,
                        focusNode: controller.birthdayFocusNode,
                        validator: (String? value) => controller.dateValidate(value),
                        formatters: [controller.dateMaskFormatter],
                        keyboardType: TextInputType.number,
                      ),
                      const SizedBox(height: 12),
                      getIt<AppDecoration>().input(
                        context,
                        controller: controller.textCitizenshipEditingController,
                        text: 'Гражданство',
                        key: controller.citizenshipKey,
                        focusNode: controller.citizenshipFocusNode,
                        validator: (String? value) => controller.textValidate(value),
                      ),
                      const SizedBox(height: 12),
                      getIt<AppDecoration>().input(
                        context,
                        controller: controller.textPassportNumberEditingController,
                        text: 'Номер загранпаспорта',
                        key: controller.passportNumberKey,
                        focusNode: controller.passportNumberFocusNode,
                        validator: (String? value) => controller.textValidate(value),
                        keyboardType: TextInputType.number,
                      ),
                      const SizedBox(height: 12),
                      getIt<AppDecoration>().input(
                        context,
                        controller: controller.textExpirationDateEditingController,
                        text: 'Срок действия загранпаспорта',
                        key: controller.expirationDateKey,
                        focusNode: controller.expirationDateFocusNode,
                        validator: (String? value) => controller.dateValidate(value),
                        formatters: [controller.dateMaskFormatter],
                        keyboardType: TextInputType.number,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  String getTitle(int index) {
    List<String> prefix = [
      'Первый',
      'Второй',
      'Третий',
      'Четвертый',
      'Пятый',
      'Шестой',
      'Седьмой',
      'Восьмой',
      'Девятый',
      'Десятый'
    ];

    return index < prefix.length ? '${prefix[index].toString()} турист' : '${(index + 1).toString()}-й турист';
  }
}
